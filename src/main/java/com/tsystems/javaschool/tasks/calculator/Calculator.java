package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    /*
    Main idea of solution:
    1. go forward char by char and detect all allowable symbols in this calculator and numbers
    2. once valid symbol and number is detected, this composition(digit) goes to 'calculator' function for processing
    3. in case if digit is valid it,s added into accepted digit list for final result evaluation
    4. in case if '(' or ')' is met then we call recursion call and try to get valid digit from bracket set
    5. if result from brackets is valid it,s added to accepted digits from latest recursion stack
    6. recursion repeats up to ')' is met
    7. We keep track on latest char position through while loop AND all recursion calls if brackets exists
     */

    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty())return null;
        char[] validSymbols = {'-', '+', '*', '/', '.', '(', ')'};
        int currentCharPosition = 0;
        int bracketBalance = 0; // keep trace if brackets are closed or not, if at the end of statement balance != 0, null to be returned!
        List<Integer> OrderedCurrentChar = new ArrayList<>(); // list of currentChars came from recursion (list is used when brackets inside expression )
        Double result = getResult(statement,currentCharPosition,bracketBalance,validSymbols, OrderedCurrentChar);
        // if result is valid double, then convert it to String
        return result != null ? getStringFromDouble(result) : null;
    }

    private Double getResult(String statement, int currentCharPosition, int bracketBalance, char[] validSymbols, List<Integer> OrderedCurrentChar){
        int dotCounter = 0;
        int symbolCounter = 0;
        StringBuilder builder = new StringBuilder();
        List<Double> acceptedDigits = new ArrayList<>(); // list of accepted digits after iteration, it,s needed for summation of them before result return
        while (currentCharPosition<statement.length()){
            char c = statement.charAt(currentCharPosition);
            char insideLambda = c; // used as immutable variable inside lambda! Inside stream, we check if current char is in valid list
            if (!Character.isDigit(c) && IntStream.range(0,validSymbols.length).noneMatch(i-> insideLambda == validSymbols[i])){return null;}

            while (c == '-' || c == '+' || c == '*' || c == '/'){
                builder.append(c);
                symbolCounter++;currentCharPosition++;
                if (symbolCounter>1)return null;
                if (currentCharPosition<statement.length()){
                    c = statement.charAt(currentCharPosition);
                } else break;
            }
            while (Character.isDigit(c) || c == '.'){ // check if current char is digit or .
                builder.append(c);
                dotCounter = !Character.isDigit(c) ? dotCounter+1 : dotCounter;
                currentCharPosition++;
                if (dotCounter>1)return null;
                if (currentCharPosition<statement.length()){
                    c = statement.charAt(currentCharPosition);
                } else break;
            }

            if (c == '('){ // if open bracket is met, then we go inside recursive loop!
                Optional<Double> resultFromBrackets = Optional.ofNullable(getResult(statement,++currentCharPosition,++bracketBalance,validSymbols,OrderedCurrentChar));
                if (resultFromBrackets.isPresent()){ // if result from recursive loop is not null, then we include it for further calculation
                    builder.append(getStringFromDouble(resultFromBrackets.get()));
                    --bracketBalance; // set bracket balance
                    // in order to keep latest char position in statement,
                    // it is important to get biggest value from saved positions from previous recursive calls!
                    currentCharPosition = OrderedCurrentChar.stream().sorted().collect(Collectors.toList()).get(OrderedCurrentChar.size()-1);
                    c = currentCharPosition<statement.length() ? statement.charAt(currentCharPosition) : c; // get next char in statement and continue
                }else return null;
            }

            char insideLambda1 = c; // check if no illegal symbol is met in stream of valid list (probably it is not optimal solution here!)
            if (!Character.isDigit(c) && IntStream.range(0,validSymbols.length).noneMatch(i-> insideLambda1 == validSymbols[i])){return null;}
            double acceptedDigit = calculation(builder.toString(),acceptedDigits); // calculate new accepted digit if it,s valid
            builder.setLength(0); symbolCounter = 0; dotCounter = 0; // set variables for new iteration in while loop
            if (!Double.isNaN(acceptedDigit)){
                acceptedDigits.add(acceptedDigit); // add new valid calculated digit in list
            } else return null;

            if (c == ')'){ // if closed bracket is met, add current char position in list to keep track on it during recursion calls!
                OrderedCurrentChar.add(++currentCharPosition);
                --bracketBalance;break; // set bracket balance BEFORE exit from loop:
                // it is needed when current char position is latest in statement and balance must be checked before return call, see below!
            }
        }
        // return final result: if we reach end of statement AND check bracket balance! if this is OK, return summation of accepted digits
        return currentCharPosition>statement.length()-1 && bracketBalance !=0 ? null : acceptedDigits.stream().reduce((double) 0,Double::sum);
    }

    /*
    function for calculation of the accepted digit from statement, it takes valid digit with any valid symbol BEFORE digit
     */
    private double calculation(String numberCandidate, List<Double> acceptedDigits) {
        int listCount = acceptedDigits.size();
        boolean isNumber = isNumber(numberCandidate); // check is input string is number (accepts negative digits too!)
        if (isNumber){
            return Double.parseDouble(numberCandidate);
        }else { // if input string is not number, then take first char and rest of the string for further verification
            char firstChar = 'n'; String numberTail=""; // just initiation of the variables!
            if (numberCandidate.length()>1){
                firstChar = numberCandidate.charAt(0);
                numberTail = numberCandidate.substring(1);
                isNumber = isNumber(numberTail);
            }
            if (firstChar == '+' && isNumber){ // case when: +12.53 as example, it,s valid, return it.
                return Double.parseDouble(numberTail);
            }
            // before going for multiply or division cases, we should take latest accepted digit from list
            double previousAcceptedDigit = listCount == 0 ? Double.NaN : acceptedDigits.remove(listCount-1);
            // check not valid case: *43 or /21, means that symbols '*', '/' aren,t allowed at the beginning of expression!
            if ((firstChar == '*' || firstChar == '/') && Double.isNaN(previousAcceptedDigit)) {
                return Double.NaN;
            }else {
                if (firstChar == '*' && isNumber) { // case: prevDigit * validInputDigit, returns multiplied result
                    return previousAcceptedDigit * Double.parseDouble(numberTail);
                }
                if (firstChar == '/' && isNumber) { // before division, check if valid input digit is not zero
                    String zeroDivisionCheck = getStringFromDouble(Double.parseDouble(numberTail));
                    if (zeroDivisionCheck.equals("0")) return Double.NaN;
                    return previousAcceptedDigit / Double.parseDouble(numberTail);
                }
            }
        }
        return Double.NaN;
    }

    private boolean isNumber(String input){
        return input.matches("-?\\d*(\\.\\d+)?$");
    }

    private String getStringFromDouble(double result){
        DecimalFormat format = new DecimalFormat("0.#");
        return String.valueOf(result).matches("^-?[0-9]+(\\.[0]+)?$")
                ? format.format(result) : String.valueOf(result);
    }
}