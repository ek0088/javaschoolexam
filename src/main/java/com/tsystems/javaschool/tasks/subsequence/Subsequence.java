package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int nextX = 0;
        int nextY = 0;
        try{
            while (nextY<y.size() && nextX<x.size()){
                nextX = y.get(nextY++).equals(x.get(nextX)) ? nextX+1 : nextX;
            }
            return nextX == x.size();
        }catch (RuntimeException exception){
            throw new IllegalArgumentException();
        }
    }
}