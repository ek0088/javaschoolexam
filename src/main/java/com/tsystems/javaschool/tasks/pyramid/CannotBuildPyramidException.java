package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    CannotBuildPyramidException(String message, RuntimeException exception){
        super(message, exception);
    }
}