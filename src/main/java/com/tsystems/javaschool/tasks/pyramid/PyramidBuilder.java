package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        try{
            inputNumbers = inputNumbers.stream().sorted().collect(Collectors.toList());
            int[][] resultedMatrix = sortedMatrix(inputNumbers); // create empty resulted matrix with defined size (rows and columns)
            int offset = 0; // skip zeros in front and tail sides on every row before iteration
            int currentInputElement = inputNumbers.size()-1; // element position in input sorted list
            int currentRow = resultedMatrix.length-1; // take current row in resulted matrix
            int currentColumn = resultedMatrix[currentRow].length-1; // take current column in resulted matrix count
            while (currentRow>-1){
                int currentPosition = currentColumn-offset; // takes element positions with zeros offset in every row
                while (currentPosition>offset-1 && currentInputElement>-1){
                    // put elements for every row
                    resultedMatrix[currentRow][currentPosition] = inputNumbers.get(currentInputElement--);
                    currentPosition -= 2; // every second position in row is being filled by number
                }
                currentRow--; offset++;
            }
            return resultedMatrix;
        }catch (RuntimeException exception){ // wrap any unchecked exception here
            throw new CannotBuildPyramidException("pyramid cannot be built!",exception);
        }
    }

    private int[][] sortedMatrix(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if (inputNumbers.size()>254){ // array length constrained by 254 elements acc to test case
            throw new CannotBuildPyramidException("pyramid cannot be built => too many elements in list!", null);
        }
        int inputLength = inputNumbers.size();
        int rows = 0;
        while (inputLength>0){
            inputLength -= ++rows;
        }
        return new int[rows][rows*2-1];
    }

}